/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9144719340250067, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.15444015444015444, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9992685714285714, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.7770611329220415, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7744107744107744, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.06766917293233082, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.7605820105820106, 500, 1500, "me"], "isController": false}, {"data": [0.7933450087565674, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.6903669724770642, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.755602665051484, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 30072, 0, 0.0, 498.83552806597675, 11, 14389, 70.0, 417.90000000000146, 2993.9500000000007, 8684.990000000002, 97.49771428941959, 255.5495382428965, 98.69873501218235], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 518, 0, 0.0, 2893.5714285714307, 714, 9191, 2118.5, 5809.4000000000015, 6922.699999999997, 8352.009999999998, 1.7188231039025248, 1.0860142072509116, 2.126707883441893], "isController": false}, {"data": ["getLatestMobileVersion", 21875, 0, 0.0, 68.13613714285623, 11, 1470, 49.0, 131.0, 172.0, 263.0, 72.93417086767491, 48.78897172300519, 53.774706059662655], "isController": false}, {"data": ["findAllConfigByCategory", 1783, 0, 0.0, 838.8474481211431, 35, 8375, 215.0, 2876.0000000000027, 4297.599999999999, 6676.440000000002, 5.944165516505644, 6.722015300892124, 7.720449352492682], "isController": false}, {"data": ["getNotifications", 891, 0, 0.0, 1679.0718294051621, 72, 9103, 286.0, 8133.6, 8523.4, 8941.16, 2.9698614064676985, 24.689873196542493, 3.3033907636393636], "isController": false}, {"data": ["getHomefeed", 266, 0, 0.0, 5634.195488721806, 935, 11485, 6380.0, 9037.7, 9447.5, 11271.409999999996, 0.8826359624381989, 10.754539944669343, 4.416627608919269], "isController": false}, {"data": ["me", 1134, 0, 0.0, 1319.6022927689598, 76, 8966, 276.5, 5615.0, 7321.5, 8430.2, 3.779811009449528, 4.974360656967152, 11.978014380530974], "isController": false}, {"data": ["findAllChildrenByParent", 1142, 0, 0.0, 1310.1208406304747, 66, 9087, 247.0, 6543.200000000001, 7877.85, 8674.13, 3.8066539778200736, 4.275050853997154, 6.081724519251603], "isController": false}, {"data": ["getAllClassInfo", 654, 0, 0.0, 2288.389908256879, 76, 9181, 372.5, 8599.5, 8780.75, 9070.95, 2.1793461961411573, 6.2663980753690565, 5.595215966460063], "isController": false}, {"data": ["findAllSchoolConfig", 1651, 0, 0.0, 906.1187159297403, 60, 8559, 253.0, 2956.3999999999996, 4385.4, 7038.76, 5.503333333333333, 120.0199609375, 4.036136067708333], "isController": false}, {"data": ["getChildCheckInCheckOut", 158, 0, 0.0, 9721.67088607595, 6666, 14389, 9728.0, 10507.6, 10687.9, 12962.969999999992, 0.5122585414248569, 34.15473819900596, 2.357189694525318], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 30072, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
